//
//  ViewController.swift
//  apppokemon
//
//  Created by USRDEL on 13/6/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var pokemon:[Pokemon]=[]
    var selectedIndex:Int = 0
    override func viewDidLoad() {
        self.tableView.delegate = self
        super.viewDidLoad()
        let networking = Networking()
        networking.getAllPokemon {(pokemonArray) in
        self.pokemon = pokemonArray
        self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pokemon1 = pokemon[selectedIndex]
        let destination = segue.destination as! DetalleViewController;
        destination.pokemon = pokemon1
    }
    func numberOfSection(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = UITableViewCell()
       cell.textLabel?.text = pokemon[indexPath.row].name
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "toDetalleSegue", sender: self)
    }

}

