//
//  Pokemon.swift
//  apppokemon
//
//  Created by USRDEL on 13/6/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import Foundation

struct Pokemon:Decodable {
    var id:Int
    var name:String
    var weight:Int
    var height:Int
    var sprites:Sprite
}

struct Sprite:Decodable {
    var defaultSprite:String
    
    enum CodingKeys: String, CodingKey{
    case defaultSprite = "front_default"
    }
}


