//
//  DetalleViewController.swift
//  apppokemon
//
//  Created by USRDEL on 20/6/18.
//  Copyright © 2018 USRDEL. All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {

    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    var pokemon:Pokemon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = pokemon.name
        weightLabel.text = "\(pokemon.weight)"
        heightLabel.text = "\(pokemon.height)"
       
        
        let network = Networking()
        network.getImage(pokemon.id) { (image) in
            self.imageView.image = image
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
